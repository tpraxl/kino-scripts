#!/usr/bin/env bats
# Copyright 2020 Kino Traumstern
# SPDX-License-Identifier: Apache-2.0

load helpers
load assert
load ../bash/settings

setup() {
  wipe_working_directory
  wipe_remote_folder
  create_remote_production_folder
}

CMD="/code/bash/update-website.sh"

@test "lsbackup informs the user and issues the expected ssh command" {
  create_remote_folder $(get_backup_folder_name '2020-01-01')
  run $CMD lsbackup

  assert_successful_status $status
  assert_output_contains "list backup"
  assert_output_contains "ssh traumstern ls \.kino-backups/content/"
}

@test "lsfailed informs the user and issues the expected ssh command" {
  create_remote_folder ".kino-failed-2020-01-01"
  run $CMD lsfailed

  assert_successful_status $status
  assert_output_contains "list failed"
  assert_output_contains "ssh traumstern ls -d \.kino-failed-\*"
}

@test "rm informs the user, and removes the given backup" {
  local issued_backup_name="my-backup" expected_folder
  expected_folder="$(get_backup_folder_name ${issued_backup_name})"
  create_remote_folder "${expected_folder}"

  run $CMD rmbackup "${issued_backup_name}"

  assert_successful_status $status

  assert_not_equals "${issued_backup_name}" "${expected_folder}"

  assert_output_contains "removing folder ${expected_folder}"
  assert_output_contains "ssh traumstern rm -r ${expected_folder}"
}

@test "It shows usage information when issued without subcommand" {
  run $CMD
  assert_usage_information_shown
}

assert_usage_information_shown() {
  assert_successful_status "${status}"
  assert_output_contains "Usage"
  assert_output_contains "update"
  assert_output_contains "rollback <backup>"
  assert_output_contains "rmbackup <name>"
  assert_output_contains "lsbackup"
  assert_output_contains "lsfailed"
}

@test "It shows usage information when issued with an unknown subcommand" {
  run $CMD unknown
  assert_usage_information_shown
}

@test "It shows usage information when issued with the help subcommand" {
  run $CMD help
  assert_usage_information_shown
}


