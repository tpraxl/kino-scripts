#!/usr/bin/env bats
# Copyright 2020 Kino Traumstern
# SPDX-License-Identifier: Apache-2.0

load helpers
load assert
load ssh_assert
load ../bash/settings

setup() {
  wipe_working_directory
  wipe_remote_folder
  create_remote_production_folder
}

CMD="/code/bash/update-website.sh"

@test "rollback informs the user" {
  local issued_rollback_target="my-backup"
  create_remote_backup_folder "${issued_rollback_target}"

  run $CMD rollback "${issued_rollback_target}"

  assert_successful_status $status
  assert_output_contains "rollback to ${issued_rollback_target}"
}

create_remote_backup_folder() {
  local name=${1}
  create_remote_folder "${name}"
  create_remote_file "${name}/file"
}

@test "rollback first backs up the production folder" {
  local issued_rollback_target="my-backup" current_date_formatted
  create_remote_backup_folder "${issued_rollback_target}"

  run $CMD rollback "${issued_rollback_target}"

  current_date_formatted=$(get_date_formatted)
  assert_ssh_moved ".kino" ".kino-failed-${current_date_formatted}"
}

@test "rollback moves the target to the production folder" {
  local issued_rollback_target="my-backup"
  create_remote_backup_folder "${issued_rollback_target}"

  run $CMD rollback "${issued_rollback_target}"

  assert_ssh_moved "my-backup" ".kino"
}

@test "production folder is restored in case of an error" {
  local issued_rollback_target="non-existing-rollback-target"

  run $CMD rollback "${issued_rollback_target}"

  local current_date_formatted
  current_date_formatted=$(get_date_formatted)

  assert_ssh_moved ".kino-failed-${current_date_formatted}" ".kino"
}

@test "old .kino contents are removed afterwards" {
  create_remote_file .kino/a-file.xml
  create_remote_folder .kino/a-dir
  create_remote_file .kino/a-dir/with-a-file.xml

  local issued_rollback_target="my-backup"
  create_remote_backup_folder "${issued_rollback_target}"
  run $CMD rollback "${issued_rollback_target}"
  assert_remote_file_not_present ".kino/a-file.xml"
  assert_remote_file_not_present ".kino/a-dir/with-a-file.xml"
}

@test ".kino-failed folder is deleted after successful rollback" {
  local issued_rollback_target="my-backup" current_date_formatted
  create_remote_backup_folder "${issued_rollback_target}"

  run $CMD rollback "${issued_rollback_target}"

  current_date_formatted=$(get_date_formatted)
  assert_remote_folder_not_present ".kino-failed-${current_date_formatted}"
}
