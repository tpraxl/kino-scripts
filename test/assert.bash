#!/usr/bin/env bash
# Copyright 2020 Kino Traumstern
# SPDX-License-Identifier: Apache-2.0

assert_successful_status() {
  local status=${1}
  assert_equals 0 "$status"
}

assert_error_status() {
  local status=${1}
  assert_not_equals 0 "$status"
}

assert_equals() {
  local expected=${1}
  local actual=${2}
  [[ "$expected" == "$actual" ]]
}

assert_not_equals() {
  local expected=${1}
  local actual=${2}
  [[ "$expected" != "$actual" ]]
}

assert_file_present() {
  local file=${1}
  [[ -f  "${file}" ]]
}

assert_file_not_present() {
  local file=${1}
  [[ ! -f  "${file}" ]]
}

assert_folders_present() {
  local expected_folders=("${@}")

  for folder in "${expected_folders[@]}"; do
    [[ -d  "${folder}" ]]
  done
}

assert_folders_not_present() {
  local expected_folders=("${@}")

  for folder in "${expected_folders[@]}"; do
    [[ ! -d  "${folder}" ]]
  done
}

assert_output_contains() {
  assert_string_in_lines "${1}" "${output[@]}"
}
assert_string_in_lines() {
  local string=${1}
  local lines=("${@:2}")
  local lines_as_string
  lines_as_string=$(printf "%s\n" "${lines[@]}")

  [[ "$lines_as_string" =~ $string ]]
}

assert_output_not_contains() {
  assert_string_not_in_lines "${1}" "${output[@]}"
}

assert_string_not_in_lines() {
  local string=${1}
  local lines=("${@:2}")
  local lines_as_string
  lines_as_string=$(printf "%s\n" "${lines[@]}")

  ! [[ "$lines_as_string" =~ $string ]]
}

assert_remote_file_not_present() {
  local remote_file=${1}
  assert_file_not_present ${REMOTE_FIXTURE:?}/${remote_file}
}

assert_remote_folder_not_present() {
  local remote_folder=${1}
  assert_folders_not_present ${REMOTE_FIXTURE:?}/${remote_folder}
}
