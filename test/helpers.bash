#!/usr/bin/env bash
# Copyright 2020 Kino Traumstern
# SPDX-License-Identifier: Apache-2.0

print() {
  print_hint "$@"
}

print_hint() {
  (>&3 echo -e "\033[93m${1}\033[0m")
}

print_error() {
  (>&3 echo -e "\033[91m${1}\033[0m")
}

create_remote_production_folder() {
  create_remote_folder ".kino"
}

create_remote_folder() {
  local remote_folder=${1}
  mkdir -p "${REMOTE_FIXTURE}/${remote_folder}"
}

create_remote_file() {
  local remote_file=${1}
  setup_empty_files "${REMOTE_FIXTURE}" "${remote_file}"
}

create_files_locally_and_remote() {
  local local_folder="${1}"
  local remote_folder="${2}"
  local files=("${@:3}")
  setup_empty_files "${local_folder}" "${files[@]}"
  setup_empty_files "${REMOTE_FIXTURE}/${remote_folder}" "${files[@]}"
}

setup_empty_files() {
  local folder="${1}"
  [[ ! -d "${folder}" ]] && mkdir -p "${folder}"
  printf "${folder}/%s\n" "${@:2}" | xargs touch
}

wipe_working_directory() {
  clean_folder "${WORKDIR}"
}

wipe_remote_folder() {
  clean_folder "${REMOTE_FIXTURE}"
}

clean_folder() {
  local folder=${1}
  # deletes all files, also dotfiles, but not . and ..
  rm -r "${folder:?}"/{,.[!.],..?}* || return 0
}
