#!/usr/bin/env bats
# Copyright 2020 Kino Traumstern
# SPDX-License-Identifier: Apache-2.0

load helpers
load assert
load ssh_assert
load ../bash/settings

setup() {
  wipe_working_directory
  wipe_remote_folder
  create_remote_production_folder
}

CMD="/code/bash/update-website.sh"

@test "it creates del/* and com/* working folders" {
  run $CMD update
  local expected_folders=(
    "del"
    "del/documents"
    "del/images_"
    "com/documents"
    "com/images_"
  )
  assert_folders_present "${expected_folders[@]}"
}

@test "it moves deletable Pest images to del working folders" {
  local src_folder="delete/images"
  local dst_folder="del/images_"
  local expected_files=("image1.jpg" "image2.jpg")

  assert_update_has_moved_files "${src_folder}" "${dst_folder}" "${expected_files[@]}"
}

assert_update_has_moved_files() {
  local src_folder=${1}
  local dst_folder=${2}
  local expected_files=("${@:3}")

  setup_empty_files "${src_folder}" "${expected_files[@]}"

  run $CMD update

  assert_files_moved "${src_folder}" "${dst_folder}" "${expected_files[@]}"
}

assert_files_moved() {
  local src_folder=${1}
  local dst_folder=${2}
  local expected_files=("${@:3}")

  local file
  for file in "${expected_files[@]}"; do
    assert_file_not_present "${src_folder}/${file}"
    assert_file_present "${dst_folder}/${file}"
  done
}

@test "It removes deletable images from the server" {
  local expected_files=("image1.jpg" "image2.jpg")

  create_files_locally_and_remote "delete/images" ".kino/images_" "${expected_files[@]}"

  run $CMD update

  assert_ssh_removed ".kino/images_" "${expected_files[@]}"
}

@test "it moves deletable Pest documents to del working folders" {
  local src_folder="delete"
  local dst_folder="del/documents"
  local expected_files=("traumstern_doc_186.xml" "traumstern_doc_263.xml")

  assert_update_has_moved_files "${src_folder}" "${dst_folder}" "${expected_files[@]}"
}

@test "It removes deletable documents from the server" {
  local expected_files=("traumstern_doc_186.xml" "traumstern_doc_263.xml")

  create_files_locally_and_remote "delete" ".kino/documents" "${expected_files[@]}"

  run $CMD update

  assert_ssh_removed ".kino/documents" "${expected_files[@]}"
}

@test "it moves committable Pest images to com working folders" {
  local src_folder="commit/images"
  local dst_folder="com/images_"
  local expected_files=("image1.jpg" "image2.jpg")

  assert_update_has_moved_files "${src_folder}" "${dst_folder}" "${expected_files[@]}"
}

@test "It uploads committable images to the server" {
  local src_folder="commit/images"
  local local_workdir="com/images_"

  local expected_files=("image1.jpg" "image2.jpg")

  setup_empty_files "${src_folder}" "${expected_files[@]}"

  run $CMD update

  assert_scp_uploaded "${local_workdir}" "${expected_files[@]}"
}

assert_scp_uploaded() {
  local folder
  folder="$(append_slash_if_not_empty ${1})"
  local expected_files=("${@:2}")

  local file
  for file in "${expected_files[@]}"; do
    assert_string_in_lines "scp\ .*${folder}${file}" "${output[@]}"
  done
}

append_slash_if_not_empty() {
  [[ -z "${1}" ]] && echo "" || echo "${1}/"
}

@test "it moves commitable Pest documents to com working folders" {
  local src_folder="commit"
  local dst_folder="com/documents"
  local expected_files=("traumstern_doc_186.xml" "traumstern_doc_263.xml")

  assert_update_has_moved_files "${src_folder}" "${dst_folder}" "${expected_files[@]}"
}

@test "It uploads committable documents to the server" {
  local src_folder="commit/"
  local local_workdir="com/documents"

  local expected_files=("traumstern_doc_186.xml" "traumstern_doc_263.xml")

  setup_empty_files "${src_folder}" "${expected_files[@]}"

  run $CMD update

  assert_scp_uploaded "${local_workdir}" "${expected_files[@]}"
}

@test "It uploads navigation.xml to the server" {
  local src_folder="./"

  local expected_files=("navigation.xml")

  setup_empty_files "${src_folder}" "${expected_files[@]}"

  run $CMD update

  assert_scp_uploaded "" "${expected_files[@]}"
}

@test "it continues the update when encountering files to delete, not present on the remote site" {
  local removed_images=("a1.jpg" "z3.jpg")
  local removed_documents=("a.xml" "z.xml")
  local missing_images=("b2.jpg" "y4.jpg")
  local missing_documents=("b.xml" "y.xml")
  local added_images=("c.jpg")
  local added_documents=("c.xml")

  create_files_locally_and_remote "delete/images" ".kino/images_" "${removed_images[@]}"
  create_files_locally_and_remote "delete/" ".kino/documents" "${removed_documents[@]}"
  setup_empty_files "delete/images" "${missing_images[@]}"
  setup_empty_files "delete/" "${missing_documents[@]}"
  setup_empty_files "commit/images" "${added_images[@]}"
  setup_empty_files "commit/" "${added_documents[@]}"

  run $CMD update

  assert_ssh_removed ".kino/images_" "${removed_images[@]}"
  assert_ssh_removed ".kino/documents" "${removed_documents[@]}"
  assert_ssh_not_removed ".kino/images_" "${missing_images[@]}"
  assert_ssh_not_removed ".kino/documents" "${missing_documents[@]}"
  assert_scp_uploaded "com/images_" "${added_images[@]}"
  assert_scp_uploaded "com/documents" "${added_documents[@]}"
}

@test "It creates a backup before it updates" {
  run_complete_update
  assert_backup_created "$(get_date_formatted)"
}

run_complete_update() {
  local src_folder="./"

  local expected_files=("navigation.xml")

  setup_empty_files "commit/" "traumstern_doc_186.xml"
  setup_empty_files "delete/" "traumstern_doc_436.xml"
  setup_empty_files "commit/images" "image1.jpg"
  setup_empty_files "delete/images" "image2.jpg"

  setup_empty_files "${src_folder}" "${expected_files[@]}"

  run $CMD update
}

assert_backup_created() {
  local backup_name="${1}"
  assert_folders_present "/remote/.kino-backups/content/${backup_name}"
}

@test "It creates one daily backup before it updates" {
  run_complete_update
  local date
  date="$(date +%d-%m-%Y)"
  assert_backup_created "${date}"
}

@test "It copies the contents of .kino, not .kino into the backup folder" {
  run_complete_update
  local date backup_folder_target
  date="$(date +%d-%m-%Y)"
  backup_folder_target=$(get_backup_folder_name "${date}")
  assert_remote_folder_not_present "${backup_folder_target}/.kino"
}

@test "Two subsequent calls on the same day also copy the contents of .kino, not the folder .kino" {
  run_complete_update
  run_complete_update
  local date backup_folder_target
  date="$(date +%d-%m-%Y)"
  backup_folder_target=$(get_backup_folder_name "${date}")
  assert_remote_folder_not_present "${backup_folder_target}/.kino"
}
