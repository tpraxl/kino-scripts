= Skriptsammlung

Das hier wird eine kleine Skriptsammlung für's Kino-Traumstern.
Die Skripte werden nach Sprache gesammelt.

== update-website

=== Testing

==== Manuell

Zum Testen wird https://github.com/bats-core/bats-core[bats-core]
verwendet.

Das link:bash/update-website.sh[update-website.sh Script] erstellt
Verzeichnisse und bewegt Dateien. Um Dein System nicht zu
verändern, laufen die Tests in einem Docker Container.

Den kannst Du mit `./bats-docker -b` bauen. Du führst die Tests mit
`./bats-docker` aus. Mehr Informationen bekommst Du mit
`./bats-docker -h`.

==== CI

Wir benutzen GitLab CI/CD mit link:.gitlab-ci.yml[.gitlab-ci.yml].
Teil des Prozesses ist die automatische Image-Erstellung und Testausführung.
Es ist kein manuelles Bauen oder Bereitstellen von CI Images erforderlich.
