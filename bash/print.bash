#!/usr/bin/env bash
# Copyright 2020 Kino Traumstern
# SPDX-License-Identifier: Apache-2.0

exit_with_error() {
  print_error "${1}"
  exit 1
}

print_error() {
  (>&2 echo -e "\033[91m${1}\033[0m")
}

print() {
  print_hint "$@"
}

print_hint() {
  echo -e "\033[93m${1}\033[0m"
}

print_usage() {
  local global_script_path="${0}"
  local path="${1:-${global_script_path}}"
  echo "
$(cat "${path}" | awk '/^####/,/^$/')
"
}