#!/usr/bin/env bash
# Copyright 2020 Kino Traumstern
# SPDX-License-Identifier: Apache-2.0
############################################################################
#
# Version 0.1.0
#
# Usage: update-website.sh [ subcommand ]
#
# Subcommands:
#
# - update             … Pushes documents and images to server and
#                        deletes from server, according to the
#                        contents of an extracted Pest zip
#                        in the working folder
#                        Example: ./update-website.sh update
# - rollback <backup>  … Rollback production to the given backup.
#                        Example:
#                        ./update-website.sh \
#                           rollback .kino-backups/content/13-03-2020
#
# - rmbackup <name>    … removes the folder ".kino-backups/content/${name}"
#                        from the server.
# - lsbackup           … lists the backups
# - lsfailed           … lists the failed update attempts
#
############################################################################

# exit when a command fails
set -o errexit
# return the exit status of the last command that threw a non-zero exit code
set -o pipefail
# exit when script tries to use undeclared variables
set -o nounset

SCRIPT_PATH=$(realpath "${BASH_SOURCE[0]}")
# containing directory
SCRIPT_DIR=$(dirname "${SCRIPT_PATH}")

# shellcheck source=print.bash
source "${SCRIPT_DIR}/../bash/print.bash"
# shellcheck source=settings.bash
source "${SCRIPT_DIR}/../bash/settings.bash"

main() {
  local sub_command="${1:-}"
  case "${sub_command}" in
    "rollback" | "update")
      shift

      local current_date
      current_date="$(get_date_formatted)"

      sub_"${sub_command}" "${current_date}" "$@"
      ;;

    "rmbackup" | "lsbackup" | "lsfailed")
      shift
      sub_"${sub_command}" "$@"
      ;;

    *)
      print_usage "${SCRIPT_PATH}"
      ;;
  esac
}

sub_rmbackup() {
  local backup_name="${1}"
  local folder_to_delete
  folder_to_delete=$(get_backup_folder_name "${backup_name}")
  echo "removing folder ${folder_to_delete}"
  # shellcheck disable=SC2029
  ssh traumstern "rm -r ${folder_to_delete}"
}

sub_lsbackup() {
  echo "list backup"
  local folder_name
  folder_name=$(get_backup_folder_name '')
  ssh traumstern ls ${folder_name}
}

sub_lsfailed() {
  echo "list failed"
  ssh traumstern "ls -d .kino-failed-*"
}

sub_rollback() {
  local current_date=${1}
  local backup_to_rollback_to=${2}
  echo "rollback to ${backup_to_rollback_to}"
  # shellcheck disable=SC2029
  ssh traumstern "mv .kino .kino-failed-${current_date}"
  # shellcheck disable=SC2029
  ssh traumstern "mv ${2} .kino && rm -r .kino-failed-${current_date}" \
    || ssh traumstern "mv .kino-failed-${current_date} .kino"
}

sub_update() {
  local current_date="${1}"
  echo "create backup"
  backup "$current_date"
  echo "update website"
  update
}

update() {
  # Extract the Pest zip into this folder with navigation.xml, commit and delete folders directly in this folder and execute this script.
  mkdir -p del/documents del/images_ com/documents com/images_

  move_if_sources_exist delete/images/* del/images_/

  move_if_sources_exist delete/*.xml del/documents

  move_if_sources_exist commit/images/* com/images_/
  move_if_sources_exist commit/*.xml com/documents

  echo "remove deletable files"
  find del -type f | sed -n 's|^del/|.kino/|p' \
    | xargs -I{} --no-run-if-empty bash -c "ssh traumstern \"rm '{}'\" || echo 'could not delete {}' >&2"

  echo "copy files and navigation"
  scp -r com/* traumstern:.kino/
  scp navigation.xml traumstern:.kino/
}

move_if_sources_exist() {
  local sources=("${@:1:$(($# - 1))}")
  local dest=("${@: -1}")
  for file in "${sources[@]}"; do
    if [[ -e $file ]]; then
      mv $file ${dest}
    fi
  done
}

backup() {
  local current_date="${1}"
  local backup_base_folder backup_target_folder
  backup_base_folder=$(get_backup_folder_name '')
  backup_target_folder=$(get_backup_folder_name "${current_date}")
  # make sure, the backup folder base exists
  ssh traumstern "mkdir -p ${backup_base_folder}"
  # --no-target-directory to always copy the contents, not the .kino folder into the backup target
  ssh traumstern "cp -r --no-target-directory .kino ${backup_target_folder}"
}

main "$@"
